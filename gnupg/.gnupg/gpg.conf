# see: https://riseup.net/openpgp/best-practices
#      https://keyring.debian.org/creating-key.html

#---------------------
# Default key
#---------------------
## Default key to sign with otherwise, default is first key found in the secret keyring
# default-key ''

#---------------------
# Behavior
#---------------------
## Meta-data
### Don't disclose the version
no-emit-version

### Don't add additional comments
no-comments

### Force UTF-8 encoding
display-charset utf-8

## List all keys (even specified one) along with their fingerprints (use twice to print fingerprint of subkey also)
with-fingerprint
with-fingerprint

## Listing keys with policy, keyserver, validity of keys
list-options show-policy-urls show-keyserver-urls show-uid-validity show-sig-expire
verify-options show-policy-urls show-keyserver-urls show-uid-validity

### Source: https://gitweb.torproject.org/torbirdy.git/tree/gpg.conf
### Use agent (with this option gpg first tries to connect with gpg-agent then asks for passphrase)
use-agent

#-----------------------------
# keyserver
#-----------------------------
## Using hkps pools (obselete use dirmngr.conf file for keyserver and its CA certs)
# see: https://sks-keyservers.net/overview-of-pools.php#pool_hkps
# keyserver hkps://hkps.pool.sks-keyservers.net
# keyserver https://pgp.mit.edu

### never be refreshed
keyserver-options no-honor-keyserver-url

## Don't leak DNS, see https://trac.torproject.org/projects/tor/ticket/2846
# keyserver-options no-try-dns-srv
# When searching for a key with --search-keys, include keys that are marked on
## the keyserver as revoked
keyserver-options include-revoked


#-----------------------------
# Algorithm and ciphers
#-----------------------------
## Cipher preferences
### Source: https://keyring.debian.org/creating-key.html
personal-cipher-preferences AES256 AES192 AES CAST5
personal-digest-preferences SHA512 SHA384 SHA256 SHA224
default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 BZIP2 ZLIB ZIP Uncompressed
## message digest algo used when signing a key
cert-digest-algo SHA512

### Choosing good cipher for symmetric encryption
s2k-cipher-algo AES256
s2k-digest-algo SHA512

#-----------------------------
# Key settings
#-----------------------------
### prompt  for  an  expiration time
ask-cert-expire

### when outputting certificates, view user IDs from key
fixed-list-mode

### long keyids are more collision-resistant
keyid-format 0xlong


#-----------------------------
# Trust model
#-----------------------------
### https://gnupg.org/ftp/people/neal/tofu.pdf
trust-model tofu+pgp
tofu-default-policy unknown


